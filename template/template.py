#!/usr/bin/env python3



__author__          = 'Name of author'
__copyright__       = 'Copyright 2022'
__version__         = '1.0.0'
__maintainer__      = 'Name of maintainer'
__email__           = 'your@email.com'
__status__          = 'Testing'
__last_change__     = '2022-11-01'
__dependencies__    = 'None'
__about__           = 'Brief explanation about the script'



def main():
    pass


if __name__ == '__main__':
    main()
