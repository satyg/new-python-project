# New-python-project
This script creates a new project in python using a preconfigured template.

Example of executing the script 
`./new-python.py randomProject.py --path ~/Documents/python --verbose`
