#!/usr/bin/env python3

from argparse import ArgumentParser
import shutil
import os

__author__          = 'Jonas Ingemarsson'
__copyright__       = 'Copyright 2022'
__version__         = '1.0.0'
__maintainer__      = 'Jonas Ingemarsson'
__email__           = 'ingemarsson.jonas@me.com'
__status__          = 'Testing'
__last_change__     = '2022-11-06'
__dependencies__    = 'Needs to have code installed'
__about__           = 'Script to create a new python script from template'

SRC = 'template/template.py'

def main():
    ap = ArgumentParser()
    ap.add_argument('-v', '--verbose',
        default=False,
        action='store_true',
        help='Increase verbosity')
    ap.add_argument('-q', '--quiet',
        default=False,
        action='store_true',
        help='Run command silently')
    ap.add_argument('project',
        nargs='?',
        default='NewProject.py',
        help='Name of the PROJECT')
    ap.add_argument('-p', '--path',
        type=str, default='~',
        help='Where you want to create your project.')
    args = ap.parse_args()

    if args.path.endswith('/'):
        DEST = f'{args.path}{args.project}'
    else:
        DEST = f'{args.path}/{args.project}'
    shutil.copyfile(os.path.expanduser(SRC), os.path.expanduser(DEST))
    if args.verbose:
        print(f'{os.path.abspath(DEST)} has been created')
    elif args.quiet:
        pass
    else:
        print('Project created!')
    yn = input('Do you wish to open the file in code? (Y/n): ') or 'y'
    yn.lower()
    if yn == 'y':
        COMMAND = f'code {args.path}'
        os.system(os.path.expanduser(COMMAND))


if __name__ == '__main__':
    main()